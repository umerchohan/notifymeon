'use strict';

require('./welcome.scss');

var name = module.exports = 'notify-me-on.welcome';

angular
  .module(name, [])
  .config(configuration)
  .controller('Welcome', require('./welcome.controller.js'))
  .controller('Test', require('./test.controller.js'))
;

function configuration($stateProvider) {
  $stateProvider
    .state('Welcome', {
      url: '/welcome',
      template: require('./welcome.html'),
      controller: 'Welcome as vm',
      title: 'ng-Super Welcome'
    }).state('Test', {
      url: '/test',
      template: require('./test.html'),
      controller: 'Test as vm',
      title: 'Test'
    });

}
