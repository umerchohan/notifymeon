﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CricinfoScrapper.Test
{
    public class MongoDBContext
    {
        public static IMongoDatabase Database;
        static MongoDBContext()
        {
            IMongoClient client = new MongoClient(System.Configuration.ConfigurationManager.ConnectionStrings["mongoServer"].ToString());
            Database = client.GetDatabase(System.Configuration.ConfigurationManager.AppSettings["cricketDB"].ToString());
        }
        public static IMongoDatabase MongoContext => Database;

    }
}
