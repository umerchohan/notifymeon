﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ScrapySharp.Network;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CricinfoScrapper.Test
{
    public class Match
    {
        [BsonId]
        public ObjectId Id { get; set; }

        public DateTime Date { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string SeriesName { get; set; }

        public string TeamOne { get; set; }

        public string TeamTwo { get; set; }

        public string Location { get; set; }

        public int CricinfoId { get; set; }

        public string State { get; set; }

        public List<Inning> Innings { get; set; }

    }


    public class Inning
    {
        public List<BattingItem> BattingScoreCard { get; set; }
        public List<BowlingItem> BowlingScoreCard { get; set; }
    }

    public class BattingItem
    {
        public string Player { get; set; }

        public int PlayerId { get; set; }

        public string Dismissal { get; set; }

        public int Runs { get; set; }

        public int Balls { get; set; }

        public int Fours { get; set; }

        public int Sixes { get; set; }

        public string Description { get; set; }

    }

    public class BowlingItem
    {
        public string Player { get; set; }

        public int PlayerId { get; set; }

        public decimal Overs { get; set; }

        public int Maiden { get; set; }

        public int Runs { get; set; }

        public int Wickets { get; set; }

        public int Fours { get; set; }

        public int Sixes { get; set; }

        public int Zeros { get; set; }
    }   
    class Program
    {
        static void Main(string[] args)
        {
            //ExtractSchedules();

            MatchRepository repository = new MatchRepository();
            var schedules = repository.Get().Result;

            List<Match> todayMatches = schedules.Where(x =>!string.IsNullOrEmpty(x.Url)).ToList();

            for (var i = 0; i < todayMatches.Count; i++)
            {
                var match = todayMatches[i];
                var url = match.Url + "?view=scorecard;wrappertype=none;xhr=1";
                ScrapingBrowser browser = new ScrapingBrowser();

                WebPage matchPage = browser.NavigateToPage(new Uri(url));
                var elm = matchPage.Html.Descendants("h6").FirstOrDefault();
                if (elm != null)
                {
                    match.State = matchPage.Html.Descendants("h6").First().InnerText.Trim();
                }
                else
                {
                    var div = matchPage.Html.SelectNodes("//div[contains(@class, 'innings-requirement')]").FirstOrDefault();
                    match.State = div.InnerText.Trim();
                }
                var tables = matchPage.Html.Descendants("table").ToList();
                tables.Remove(tables[tables.Count - 1]);
                tables.Remove(tables[tables.Count - 1]);
                if (match.Innings == null)
                    match.Innings = new List<Inning>();
                var inningCount = tables.Count / 2;
                for (var z = match.Innings.Count; z < inningCount; z++)
                {
                    match.Innings.Add(new Inning());
                }

                int start = match.Innings.Count(x => x.BattingScoreCard != null && x.BattingScoreCard.Count > 0) == 0 ? 0 : (match.Innings.Count - 1) * 2;
                int count = match.Innings.Count - 1;
                for (var j = start; j < tables.Count; j += 2)
                {
                    var inning = match.Innings[j / 2];
                    if (inning.BattingScoreCard == null)
                    {
                        inning.BattingScoreCard = new List<BattingItem>();
                    }
                    var table = tables[j];
                    var rows = table.Descendants("tr").ToList();
                    int battingOrder = 0;
                    for (var r = 1; r < rows.Count - 1; r++)
                    {
                        var battingItem = new BattingItem();
                        var row = rows[r];
                        if (row.Attributes.Contains("class") && row.Attributes["class"].Value == "extra-wrap")
                        {
                            break;
                        }
                        if (inning.BattingScoreCard.Count < (battingOrder + 1))
                        {
                            inning.BattingScoreCard.Add(battingItem);
                        }
                        else
                        {
                            battingItem = inning.BattingScoreCard[battingOrder];
                        }


                        var childNodes = row.ChildNodes.Where(x => x.OriginalName != "#text").ToList();
                        childNodes.Remove(childNodes[0]);
                        if (childNodes.Count == 8)
                        {
                            childNodes.Remove(childNodes[3]);
                        }
                        for (var c = 0; c < childNodes.Count - 1; c++)
                        {

                            var col = childNodes[c];

                            if (c == 0)
                            {
                                battingItem.Player = col.InnerText.Replace("&dagger;", "").Replace("*", "").Trim();
                                var playerUrl = col.FirstChild.Attributes["href"].Value;
                                battingItem.PlayerId = int.Parse(playerUrl.Substring(playerUrl.LastIndexOf("/") + 1).Split('.')[0]);

                            }
                            else if (c == 1)
                            {
                                battingItem.Dismissal = col.InnerText.Trim();
                            }
                            else if (c == 2)
                            {
                                battingItem.Runs = int.Parse(col.InnerText.Trim());
                            }
                            else if (c == 3)
                            {
                                battingItem.Balls = int.Parse(col.InnerText.Trim());
                            }
                            else if (c == 4)
                            {
                                battingItem.Fours = int.Parse(col.InnerText.Trim());
                            }
                            else if (c == 5)
                            {
                                battingItem.Sixes = int.Parse(col.InnerText.Trim());
                            }
                        }
                        if (rows[r + 1].Attributes["class"].Value == "dismissal-detail")
                        {
                            battingItem.Description = rows[r + 1].ChildNodes[3].InnerText.Trim();
                            r++;
                        }

                        battingOrder++;

                    }
                    count++;
                }

                for (var j = 0; j < tables.Count; j += 2)
                {

                }
                
            }

            foreach (var match in todayMatches)
            {
                bool success=repository.Update(match.Id, match).Result;
            }
        }

        private static void ExtractSchedules()
        {
            MatchRepository repository = new MatchRepository();
            var list=repository.Get().Result;
            string[] countries = new string[] { "pakistan", "australia", "bangladesh", "england", "newzealand", "south africa", "sri lanka", "west indies", "zimbabwe" };

            Dictionary<string, List<Match>> countrySchedules = new Dictionary<string, List<Match>>();
            List<Match> schedules = new List<Match>();
            ScrapingBrowser browser = new ScrapingBrowser();

            //set UseDefaultCookiesParser as false if a website returns invalid cookies format
            //browser.UseDefaultCookiesParser = false;

            WebPage schedulePage = browser.NavigateToPage(new Uri("http://www.espncricinfo.com/ci/engine/match/index.html?view=calendar"));

            var days = schedulePage.Html.SelectNodes("//section[contains(@class, 'calender-match-detail')]");
            foreach (var day in days)
            {
                var dayElem = day.PreviousSibling.PreviousSibling;
                var strDay = dayElem.InnerText;
                var monthElem = day.ParentNode.ParentNode.PreviousSibling.PreviousSibling;
                var monthStr = monthElem.InnerText.Replace("\r\n", "").Trim();
                var strdate = strDay + " " + monthStr;
                var date = DateTime.Parse(strdate).ToUniversalTime();
                var matches = dayElem.NextSibling.NextSibling;
                var elements = matches.Descendants("a");
                if (elements.Count() > 0)
                {
                    var paragraphs = matches.Descendants("p");
                    int ct = 0;
                    foreach (var match in elements)
                    {
                        Match schedule = new Match();
                        schedule.Date = date;
                        schedule.SeriesName = paragraphs.ElementAt(ct).InnerText;
                        schedule.Name = match.InnerText.Replace("\r\n", "").Replace("\t", "").Replace("     ", "");
                        schedule.Url = "http://cricinfo.com" + match.Attributes["href"].Value;
                        schedule.CricinfoId = int.Parse(schedule.Url.Substring(schedule.Url.LastIndexOf("/") + 1).Split('.')[0]);
                        schedules.Add(schedule);
                        ct += 2;
                    }
                }
                else
                {
                    var paragraphs = matches.Descendants("p");
                    for (var i = 0; i < paragraphs.Count(); i += 2)
                    {
                        Match schedule = new Match();
                        schedule.Date = date;
                        schedule.SeriesName = paragraphs.ElementAt(i).InnerText;
                        schedule.Name = paragraphs.ElementAt(i + 1).InnerText.Replace("\r\n", "").Replace("\t", "").Replace("     ", "");
                        schedules.Add(schedule);
                    }
                }

            }
            foreach (var sch in schedules)
            {
            
                string str = sch.Name.Substring(0, sch.Name.IndexOf(" at"));
                var array = str.Split('v');
                sch.TeamOne = array[0].Trim();
                sch.TeamTwo = array[1].Trim();
                str = sch.Name.Substring(sch.Name.IndexOf(" at") + 4);
                sch.Location = str.Split(',')[0];
                if (!countrySchedules.ContainsKey(sch.TeamOne))
                    countrySchedules[sch.TeamOne] = new List<Match>();
                if (!countrySchedules.ContainsKey(sch.TeamTwo))
                    countrySchedules[sch.TeamTwo] = new List<Match>();
                countrySchedules[sch.TeamOne].Add(sch);
                countrySchedules[sch.TeamTwo].Add(sch);

                var match = list.Where(x => x.Name == sch.Name).FirstOrDefault();
                if(match==null)
                {
                    match=repository.Insert(sch).Result;
                }
                else
                {
                    sch.Id = match.Id;
                    bool success=repository.Update(match.Id, sch).Result;
                }
              
            }

           
        }
    }
}
