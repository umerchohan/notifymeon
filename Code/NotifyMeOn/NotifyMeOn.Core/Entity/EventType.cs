﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotifyMeOn.Core.Entity
{
    public class EventType
    {
        public string _id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Recurring { get; set; }

        public string CategoryId { get; set; }
        
        public bool LocationRequired { get; set; }

        public long CreatedOn { get; set; }

        public long ModifiedOn { get; set; }
    }
}
