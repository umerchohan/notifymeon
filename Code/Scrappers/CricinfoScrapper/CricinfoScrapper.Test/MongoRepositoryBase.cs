﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CricinfoScrapper.Test
{
    public class MongoRepositoryBase<T> 
    {
        readonly IMongoDatabase MongoContext;
        private string _collection = string.Empty;
        protected MongoRepositoryBase()
        {
            MongoContext = MongoDBContext.MongoContext;
            _collection = typeof(T).Name;
        }
        protected IMongoCollection<T> Collection
        {
            get
            {
                return MongoContext.GetCollection<T>(_collection);
            }
        }

   
        public async virtual Task<List<T>> Get()
        {
            return await Collection.AsQueryable().ToListAsync();
        }

        public virtual async Task<T> Get(string id)
        {
            IAsyncCursor<T> result = await Collection.FindAsync<T>(Builders<T>.Filter.Eq("_id", ObjectId.Parse(id)));
            return result.FirstOrDefault();
        }

        
        

        public virtual async Task<T> Insert(T entity)
        {
            await Collection.InsertOneAsync(entity);
            return entity;
        }

        public virtual async Task<bool> Update(ObjectId id, T entity)
        {
            UpdateOptions options = new UpdateOptions();
            options.IsUpsert = true;
            var result = await Collection.ReplaceOneAsync(Builders<T>.Filter.Eq("_id", id), entity, options);
            return result.ModifiedCount > 0 ? true : false;
        }

        public virtual async Task<bool> Delete(string id)
        {
            var result = await Collection.DeleteOneAsync(Builders<T>.Filter.Eq("_id", ObjectId.Parse(id)));
            return result.DeletedCount > 0 ? true : false;
        }

       
       
       
       
    }
}
