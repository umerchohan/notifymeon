﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotifyMeOn.Core.Entity
{
    public class BrandType
    {
        public string _id { get; set; }

        public string Name { get; set; }

    }
}
