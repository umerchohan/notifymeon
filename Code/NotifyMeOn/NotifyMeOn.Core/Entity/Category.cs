﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotifyMeOn.Core.Entity
{
    public class Category
    {
        public string _id { get; set; }

        public string ParentId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public long CreatedOn { get; set; }

        public long LastModifiedOn { get; set; }

    }
}
