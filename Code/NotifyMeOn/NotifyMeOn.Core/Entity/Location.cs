﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotifyMeOn.Core.Entity
{
    public class Location
    {
        public string _id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }

        public long CreatedOn { get; set; }

        public long LastModifiedOn { get; set; }
    }
}
